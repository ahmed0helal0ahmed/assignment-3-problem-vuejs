const app = Vue.createApp({
  data() {
    return {
      number: 0,
    };
  },
  methods: {
    add(value) {
      this.number += value;
    },
  },
  watch: {
    result(value) {
      setTimeout(() => {
        this.number = 0;
      }, 5000);
    },
  },
  computed: {
    result() {
      if (this.number < 37) {
        return "Not there yet";
      } else if (this.number > 37) {
        return "Too much!";
      } else {
        return this.number;
      }
    },
  },
});
app.mount("#assignment");
